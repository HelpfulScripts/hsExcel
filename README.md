hsExcel
========
[![npm version](https://badge.fury.io/js/hsexcel.svg)](https://badge.fury.io/js/hsexcel) 
[![Build status](https://ci.appveyor.com/api/projects/status/sw91uymqktwajoxp?svg=true)](https://ci.appveyor.com/project/HelpfulScripts/hsexcel)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/) 
[![NPM License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://www.npmjs.com/package/hsexcel) 

Helpful Scripts ES6 access to Excel files based on [xlsx](https://www.npmjs.com/package/xlsx).

## Installation
`npm i hsexcel`

See [docs](https://helpfulscripts.github.io/hsExcel/indexGH.html#!/api/hsExcel/0) for details
